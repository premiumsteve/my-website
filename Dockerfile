FROM openjdk:8-jre
ADD /target/*.jar mySite.jar
ENTRYPOINT ["java", "-jar", "mySite.jar"]
